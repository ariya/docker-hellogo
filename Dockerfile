FROM centos:centos6
ADD . /src

RUN \
  yum -y install wget && \
  wget http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm && \
  rpm -ivh epel-release-6-8.noarch.rpm

RUN yum -y install golang

EXPOSE  8200
CMD ["go", "run", "/src/serve.go"]
