Build the container (this may pull CentOS):
  sudo docker build -t hellogo .

Run the new image:
  sudo docker run -p 8200:8200 -t hellogo

Troubleshooting?
  sudo docker run -p 8200:8200 -i -t hellogo /bin/bash
