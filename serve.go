package main

import (
	"fmt"
	"log"
	"net/http"
)

func serve(w http.ResponseWriter, r *http.Request) {
	log.Println("Getting request...")
	fmt.Fprint(w, "Hello world!")
}

func main() {
	http.HandleFunc("/", serve)
	err := http.ListenAndServe(":8200", nil)
	if err != nil {
		log.Fatal(err)
	}

}
